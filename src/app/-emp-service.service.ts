import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EmpServiceService {
  API_KEY = 'test';
  constructor(private httpClient: HttpClient) { }
  getAllEmps() {
    return this.httpClient.get(`http://52.146.45.18:3000`);
  }
  postInputEmps(data) {
    return this.httpClient.post(`http://52.146.45.18:3000`, data);
  }
}
