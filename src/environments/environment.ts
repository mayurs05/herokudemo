// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC_vNfvnni0876y0iHoC29IPUI8lf7WiLc",
    authDomain: "hrmsnotification-3a10f.firebaseapp.com",
    databaseURL: "https://hrmsnotification-3a10f.firebaseio.com",
    projectId: "hrmsnotification-3a10f",
    storageBucket: "hrmsnotification-3a10f.appspot.com",
    messagingSenderId: "417755979900",
    appId: "1:417755979900:web:a194861e7c546f007469f9",
    measurementId: "G-5F4Q9EWMTV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
